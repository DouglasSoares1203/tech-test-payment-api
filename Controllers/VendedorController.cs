using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly OrganizaContext _context;

        public VendedorController(OrganizaContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var vendedor = _context.vendedores.Where(x => x.Id == id);
            if(vendedor == null)
                return NotFound();
                
            return Ok(vendedor);
        }

        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {
            var vendedor = _context.vendedores.ToList();
            return Ok(vendedor);
        }

        [HttpPost]
        public IActionResult Criar(Vendedor vendedor)
        {
            if(vendedor.Data == DateTime.MinValue)
                return BadRequest(new{Erro = "A data da tarefa não pode ser vazia"});
           
            _context.Add(vendedor);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new{id = vendedor.Id}, vendedor);
            
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Vendedor vendedor)
        {
            var vendedorBanco = _context.vendedores.Find(id);

            if(vendedorBanco == null)
                return NotFound();
            
            if(vendedor.Data == DateTime.MinValue)
                return BadRequest(new{Erro = "A data da tarefa não pode ser vazia"});

            vendedorBanco.CPF = vendedor.CPF;
            vendedorBanco.Nome = vendedor.Nome;
            vendedorBanco.Email = vendedor.Email;
            vendedorBanco.Telefone = vendedor.Telefone;
            vendedorBanco.Data = vendedor.Data;

            _context.vendedores.Update(vendedorBanco);
            _context.SaveChanges();
            return Ok(vendedorBanco);

            
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var vendedorBanco = _context.vendedores.Find(id);

            if(vendedorBanco == null)
                return NotFound();

            _context.vendedores.Remove(vendedorBanco);
            _context.SaveChanges();
            
            return NoContent();
            
        }
    }
        
    
}
