using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        
        private readonly OrganizaContext _context;

        public VendaController(OrganizaContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.vendas.Where(x => x.Id == id);
            if(venda == null)
                return NotFound();
            return Ok(venda);
        }

        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {
            var venda = _context.vendas.ToList();
            return Ok(venda);
        }

        [HttpPost("{id}")]
        public IActionResult Criar(Venda venda)
        {
            if(venda.ItensVendidos.Count < 1)
            return BadRequest("A venda precisa possuir pelo menos 1 item.");

            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new {id = venda.Id}, venda );
             
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendaBanco = _context.vendas.Find(id);

            if(venda.Status.ToString() == "AguardandoPagamento")
        {
            if (venda.Status.ToString() == "PagamentoAprovado" || venda.Status.ToString() == "Cancelada")
                vendaBanco.Id = venda.Id;
        }
        else if(venda.Status.ToString() == "PagamentoAprovado")
        {
            if (venda.Status.ToString() == "EnviadoParaTransportadora" || venda.Status.ToString() == "Cancelada")
                vendaBanco.Id = venda.Id;
        }
        else if(venda.Status.ToString() == "EnviadoParaTransportadora")
        {
            if (venda.Status.ToString() == "Entregue")
                vendaBanco.Id = venda.Id;
        }


        return Ok("Venda atualizada com sucesso!");
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var vendaBanco = _context.vendas.Find(id);

            if(vendaBanco == null)
                return NotFound();

            _context.vendas.Remove(vendaBanco);
            _context.SaveChanges();

            return NoContent();
        }


    }
}