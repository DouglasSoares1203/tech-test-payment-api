using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class OrganizaContext : DbContext
    {
        public OrganizaContext(DbContextOptions<OrganizaContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Venda>().OwnsOne(x => x.ItensVendidos);
            

        }

        public DbSet<Vendedor> vendedores { get; set; }
        public DbSet<Venda> vendas { get; set; }
        

    }
}